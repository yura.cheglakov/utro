<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('price_types', function (Blueprint $table) {
            $table->string('code')->primary();
            $table->string('title');
            $table->softDeletes();
        });

        Schema::create('offer_prices', function (Blueprint $table) {
            $table->id();
            $table->foreignUuid('offer_id')->constrained()->restrictOnDelete()->cascadeOnUpdate();
            $table->string('price_type_code');

            $table->foreign('price_type_code')->references('code')->on('price_types')->restrictOnDelete()->cascadeOnUpdate();
            $table->unsignedInteger('price');

            $table->unique(['offer_id', 'price_type_code']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('price_variants');
        Schema::dropIfExists('offer_price');
    }
};
