<?php

use App\Models\Tables\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('category_property', function (Blueprint $table) {
            $categoryForeign = $table->foreignIdFor(Category::class)->constrained()->cascadeOnUpdate()->restrictOnDelete();

            $table->string('property_code');
            $table->foreign('property_code')->references('code')->on('properties')->cascadeOnUpdate()->restrictOnDelete();

            $table->primary([Arr::first($categoryForeign->get('columns')), 'property_code']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('category_properties');
    }
};
