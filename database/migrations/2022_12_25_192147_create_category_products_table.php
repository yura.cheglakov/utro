<?php

use App\Models\Tables\Category;
use App\Models\Tables\Product;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('category_product', function (Blueprint $table) {
            $productForeign = $table->foreignIdFor(Product::class)->constrained()->cascadeOnUpdate()->cascadeOnDelete();
            $categoryForeign = $table->foreignIdFor(Category::class)->constrained()->cascadeOnUpdate()->cascadeOnDelete();

            $table->primary([Arr::first($productForeign->get('columns')), Arr::first($categoryForeign->get('columns'))]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_products');
    }
};
