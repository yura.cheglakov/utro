<?php

use App\Models\Tables\Product;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('product_property', function (Blueprint $table) {
            $productForeign = $table->foreignIdFor(Product::class)->constrained()->cascadeOnUpdate()->restrictOnDelete();

            $table->string('property_code');
            $table->foreign('property_code')->references('code')->on('properties')->cascadeOnUpdate()->restrictOnDelete();
            $table->primary([Arr::first($productForeign->get('columns')), 'property_code']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('product_options');
    }
};
