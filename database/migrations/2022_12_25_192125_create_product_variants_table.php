<?php

use App\Models\Tables\Product;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('product_variant', function (Blueprint $table) {
            $productForeign = $table->foreignIdFor(Product::class)->constrained()->cascadeOnUpdate()->cascadeOnDelete();
            $variantForeign = $table->foreignIdFor(Product::class, 'variant_id')->constrained('products')->cascadeOnDelete()->cascadeOnUpdate();

            $table->primary([Arr::first($productForeign->get('columns')), Arr::first($variantForeign->get('columns'))]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('product_variant');
    }
};
