<?php

use App\Models\Tables\Offer;
use App\Models\Tables\PropertyValue;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('offer_property_value', function (Blueprint $table) {
            $offerForeign = $table->foreignIdFor(Offer::class)->constrained()->restrictOnDelete()->cascadeOnUpdate();
            $propertyValueForeign = $table->foreignIdFor(PropertyValue::class)->constrained()->restrictOnDelete()->cascadeOnUpdate();

            $table->primary([Arr::first($offerForeign->get('columns')), Arr::first($propertyValueForeign->get('columns'))]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('offer_values');
    }
};
