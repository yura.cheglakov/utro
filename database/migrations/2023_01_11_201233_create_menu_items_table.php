<?php

use App\Models\Tables\Menu;
use App\Models\Tables\MenuItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('menu_items', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->foreignIdFor(Menu::class)->constrained()->cascadeOnUpdate()->restrictOnDelete();
            $table->foreignIdFor(MenuItem::class)->nullable()->constrained()->cascadeOnUpdate()->restrictOnDelete();
            $table->string('link')->nullable();

            $table->boolean('display_banner')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('menu_items');
    }
};
