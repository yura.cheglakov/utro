<?php

use App\Models\Tables\Product;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->unsignedBigInteger('external_id')->unique();
            $table->string('title');
            $table->string('slug');
            $table->string('article')->unique();
            $table->boolean('is_display_on_main')->default(0);
            $table->boolean('is_new')->default(0);
            $table->boolean('is_sale')->default(0);
            $table->boolean('is_preorder')->default(0);
            $table->boolean('is_hit')->default(0);
            $table->boolean('is_active')->default(0);
            $table->integer('quantity')->default(0);
            $table->jsonb('extra')->nullable();

            $table->foreignIdFor(Product::class)->constrained()->cascadeOnUpdate()->restrictOnDelete();

            $table->unique(['product_id', 'slug']);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('offers');
    }
};
