<?php

use App\Models\Tables\Product;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('external_id')->unique();
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('description')->nullable();
            $table->boolean('is_display_on_main')->default(0);
            $table->boolean('is_new')->default(0);
            $table->boolean('is_sale')->default(0);
            $table->boolean('is_preorder')->default(0);
            $table->boolean('is_hit')->default(0);
            $table->jsonb('extra')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('product_products', function (Blueprint $table) {
            $parentForeign = $table->foreignIdFor(Product::class, 'parent_id')->constrained('products')->restrictOnDelete()->cascadeOnUpdate();
            $childForeign = $table->foreignIdFor(Product::class, 'child_id')->constrained('products')->restrictOnDelete()->cascadeOnUpdate();

            $table->string('child_type');

            $table->primary([Arr::first($parentForeign->get('columns')), Arr::first($childForeign->get('columns'))]);
            $table->unique([Arr::first($parentForeign->get('columns')), Arr::first($childForeign->get('columns')), 'child_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_products');
    }
};
