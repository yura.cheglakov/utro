<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('number')->unique();
            $table->string('external_id')->nullable();

            $table->string('payment');
            $table->string('delivery');

            $table->string('buyer_first_name');
            $table->string('buyer_last_name');
            $table->string('buyer_middle_name');
            $table->string('buyer_email');
            $table->string('buyer_phone');
            $table->string('buyer_address');

            $table->unsignedInteger('amount');
            $table->unsignedInteger('delivery_amount');
            $table->unsignedInteger('cart_amount');
            $table->unsignedInteger('discount_amount');

            $table->string('payment_id')->nullable();

            $table->jsonb('cart');
            $table->jsonb('extra');

            $table->string('status');
            $table->foreign('status')->references('code')->on('statuses')->restrictOnDelete()->cascadeOnUpdate();

            $table->string('coupon')->nullable();
            $table->foreign('coupon')->references('code')->on('coupons')->restrictOnDelete()->cascadeOnUpdate();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
