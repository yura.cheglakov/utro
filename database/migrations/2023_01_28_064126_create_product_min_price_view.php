<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW product_min_price AS (select o.product_id, min(op.price) min_price from offers o join offer_prices op on o.id = op.offer_id group by 1)");
        DB::statement("create view catalog_offers as (select min(cast(o.id as varchar(100))) offer_id, o.product_id, mp.min_price from offers o join offer_prices op on o.id = op.offer_id join product_min_price mp on o.product_id = mp.product_id and op.price = mp.min_price group by 2, 3)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::statement("DROP VIEW IF EXISTS product_min_price");
        DB::statement("DROP VIEW IF EXISTS catalog_offers");
    }
};
