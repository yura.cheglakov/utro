<?php

use App\Models\Tables\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('external_id')->nullable()->unique();
            $table->string('title');
            $table->string('slug');
            $table->boolean('is_display_on_main')->default(0);
            $table->boolean('is_active')->default(0);
            $table->jsonb('extra')->nullable();

            $categoryForeign = $table->foreignIdFor(Category::class)->nullable()->constrained()->cascadeOnUpdate()->restrictOnDelete();
            $table->softDeletes();

            $table->unique([Arr::first($categoryForeign->get('columns')), 'slug']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('categories');
    }
};
