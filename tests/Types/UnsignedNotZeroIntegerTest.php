<?php

namespace Tests\Types;

use App\Types\UnsignedNotZeroInteger;
use Tests\TestCase;

class UnsignedNotZeroIntegerTest extends TestCase
{
    public function test_success()
    {
        $t = new UnsignedNotZeroInteger(1);

        $this->assertEquals(1, $t->value());
    }

    public function test_exception_zero()
    {
        $this->expectException(\UnexpectedValueException::class);
        $t = new UnsignedNotZeroInteger(0);
    }

    public function test_exception_minus()
    {
        $this->expectException(\UnexpectedValueException::class);
        $t = new UnsignedNotZeroInteger(-1);
    }
}
