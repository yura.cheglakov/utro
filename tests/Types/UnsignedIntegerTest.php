<?php

namespace Tests\Types;

use App\Types\OfferQuantity;
use Tests\TestCase;

class UnsignedIntegerTest extends TestCase
{

    public function testException()
    {
        $this->expectException(\UnexpectedValueException::class);

        new OfferQuantity(-2);
    }

    public function testSuccess()
    {
        $this->assertEquals(2, (new OfferQuantity(2))->value());
    }
}
