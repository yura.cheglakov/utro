<?php

namespace Tests\Services\Cdek\Requests;

use App\Services\Cdek\Classes\AuthorizeRequestDto;
use App\Services\Cdek\Requests\AuthorizeRequest;
use Illuminate\Support\Facades\App;
use Tests\TestCase;

class AuthorizeRequestTest extends TestCase
{
    public function test_authorize()
    {
        $dto = App::make(AuthorizeRequestDto::class);
        $request = new AuthorizeRequest($dto);
        $request->send();
    }
}
