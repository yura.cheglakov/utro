<?php

namespace Tests\Services\Cdek;

use App\Services\Cdek\CdekService;
use App\Services\Cdek\Classes\CalculateByTariffDto;
use App\Services\Cdek\Classes\Location;
use App\Services\Cdek\Classes\Package;
use App\Services\Cdek\Classes\PlannedTransferDate;
use App\Types\UnsignedInteger;
use App\Types\UnsignedNotZeroInteger;
use Tests\TestCase;

class CdekServiceTest extends TestCase
{
    public function test_cdek_calculate()
    {
        $service = new CdekService();
        $response = $service->calculate(new CalculateByTariffDto(
            new PlannedTransferDate(now()->addDay()),
            136,
            new Location(postalCode: 610010),
            new Location(postalCode: 460000),
            collect([new Package(new UnsignedNotZeroInteger(1000), new UnsignedInteger(200), new UnsignedInteger(200))]),
        ));
    }
}
