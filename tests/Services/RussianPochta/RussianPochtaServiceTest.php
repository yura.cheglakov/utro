<?php

namespace Tests\Services\RussianPochta;

use App\Services\RussianPochta\Classes\CalculateTariffDto;
use App\Services\RussianPochta\Exceptions\ExceededMaximumWeightException;
use App\Services\RussianPochta\Exceptions\RussianPochtaServiceException;
use App\Services\RussianPochta\RussianPochtaService;
use Tests\TestCase;

class RussianPochtaServiceTest extends TestCase
{
    /**
     * @throws ExceededMaximumWeightException
     * @throws RussianPochtaServiceException
     */
    public function testCalculate(): void
    {
        $service = new RussianPochtaService();
        $price = $service->calculate(new CalculateTariffDto(23020, 460000, 610010, 2500, 10, 250000, [18, 36]));

        $this->assertGreaterThan(0, $price);
    }

    /**
     * @throws RussianPochtaServiceException
     */
    public function testMaxWeight(): void
    {
        $this->expectException(ExceededMaximumWeightException::class);

        $service = new RussianPochtaService();
        $service->calculate(new CalculateTariffDto(23020, 460000, 610010, 25000, 10, 250000, [18, 36]));
    }
}
