<?php

namespace Tests\Utilities\Delivery;

use App\Services\RussianPochta\Exceptions\ExceededMaximumWeightException;
use App\Utilities\Delivery\Classes\Address;
use App\Utilities\Delivery\Classes\Bound;
use App\Utilities\Delivery\Classes\Package;
use App\Utilities\Delivery\Delivery;
use Tests\TestCase;

class DeliveryTest extends TestCase
{

    public function test_delivery_all_success()
    {
        $delivery = new Delivery();
        $results = $delivery->getAllAvailableDeliveryVariants(
            new Package(
                new Address(610010, 'Киров'),
                new Bound(400, 80, 300),
                3000,
                3,
                250000
            )
        );

        $this->assertGreaterThan(0, $results->count());
    }

    public function test_delivery_all_fail()
    {
        $this->expectException(ExceededMaximumWeightException::class);

        $delivery = new Delivery();
        $delivery->getAllAvailableDeliveryVariants(
            new Package(
                new Address(610010, 'Киров'),
                new Bound(400, 80, 300),
                300000,
                3,
                250000
            )
        );
    }
}
