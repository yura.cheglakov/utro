<?php

namespace Tests\Utilities\Delivery\Classes;

use App\Utilities\Delivery\Classes\Bound;
use Tests\TestCase;

class BoundTest extends TestCase
{

    public function test_get_volume()
    {
        $b = new Bound(20, 30, 40);

        $this->assertEquals(20, $b->getWidth());
        $this->assertEquals(30, $b->getLength());
        $this->assertEquals(40, $b->getHeight());
        $this->assertEquals(20*30*40, $b->getVolume());

        $b = new Bound(20, 40);

        $this->assertEquals(20*40, $b->getVolume());
    }
}
