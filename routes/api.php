<?php

use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\MenusController;
use App\Http\Controllers\CatalogController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/me', function (Request $request) {
    return $request->user();
});

Route::prefix('catalog')->name('catalog.')->group(function() {
    Route::get('{category?}', [CatalogController::class, 'list'])->name('list');
});

Route::prefix('categories')->name('categories.')->group(function() {
   Route::get('', [CategoriesController::class, 'list'])->name('index');
   Route::get('{category}', [CatalogController::class, 'list'])->name('list');
});

Route::prefix('menus')->name('menus.')->group(function() {
    Route::get('/{menu:code}', [MenusController::class, 'get']);
});
