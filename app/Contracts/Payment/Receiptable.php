<?php

namespace App\Contracts\Payment;

use App\Models\Tables\Order;

interface Receiptable
{
    public function buildReceipt(Order $order): Receipt;
}