<?php

namespace App\Contracts;

interface Bound
{
    /**
     * @return int
     */
    public function getWidth(): int;

    /**
     * @return int
     */
    public function getHeight(): int;

    /**
     * @return int
     */
    public function getLength(): int;

    /**
     * Получить объем
     * @return int
     */
    public function getVolume(): int;
}