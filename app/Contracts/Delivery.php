<?php

namespace App\Contracts;

use App\Contracts\Delivery\DeliveryPackage;

interface Delivery
{
    public function getAllAvailableDeliveryVariants(DeliveryPackage $package);
    public function getMinimumPrice(DeliveryPackage $package);
}