<?php

namespace App\Contracts;

interface Address
{
    /**
     * @return string|null
     */
    public function getIndex(): ?string;

    /**
     * @return string|null
     */
    public function getCity(): ?string;
}