<?php

namespace App\Contracts\Delivery;

interface DeliveryService
{
    public function calculate(DeliveryDto $dto): DeliveryResponse;
}