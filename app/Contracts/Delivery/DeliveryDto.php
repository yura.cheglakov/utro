<?php

namespace App\Contracts\Delivery;

use Illuminate\Support\Collection;

interface DeliveryDto
{
    public function toCollection(): Collection;
}