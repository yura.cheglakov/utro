<?php

namespace App\Contracts\Delivery;

use App\Contracts\Address;
use App\Contracts\Bound;

interface DeliveryPackage
{
    /**
     * @return Address
     */
    public function getAddress(): Address;

    /**
     * @return Bound
     */
    public function getBound(): Bound;

    /**
     * @return int
     */
    public function getWeight(): int;

    /**
     * @return int
     */
    public function getItemsCount(): int;

    /**
     * @return int|null
     */
    public function getDeclaredAmount(): ?int;
}