<?php

namespace App\Contracts\Delivery;

interface DeliveryRange
{
    public function getDaysMin(): int;
    public function getDaysMax(): int;
}