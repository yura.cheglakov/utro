<?php

namespace App\Contracts\Delivery;

interface DeliveryResponse
{
    public function getTitle(): string;
    public function setTitle(string $title): static;
    public function getPrice(): int;
    public function getDeliveryRange(): DeliveryRange;
}