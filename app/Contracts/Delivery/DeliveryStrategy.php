<?php

namespace App\Contracts\Delivery;

use App\Utilities\Delivery\Classes\Package;

interface DeliveryStrategy
{
    /**
     * Сумма доставки в копейках
     * @param Package $package
     * @return DeliveryResponse
     */
    public function calculate(Package $package): DeliveryResponse;

    public function getTitle(): string;

}