<?php

namespace App\Contracts\Delivery;

use Illuminate\Support\Collection;

interface HasPvz
{
    public function getPvzList(): Collection;
}