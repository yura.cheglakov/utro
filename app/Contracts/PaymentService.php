<?php

namespace App\Contracts;

use App\Models\Tables\Order;

interface PaymentService
{
    public function createPayment(Order $order);
}