<?php

namespace App\Models\Views;

use App\Models\Tables\Offer;
use App\Models\Tables\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Views\CatalogOffer
 *
 * @property-read string $offer_id
 * @property-read int $product_id
 * @property-read Product $product
 * @property-read Offer $offer
 * @method static Builder|CatalogOffer newModelQuery()
 * @method static Builder|CatalogOffer newQuery()
 * @method static Builder|CatalogOffer query()
 */
class CatalogOffer extends Model
{
    protected $perPage = 12;

    /**
     * @return string
     */
    public function getOfferId(): string
    {
        return $this->offer_id;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->product_id;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return Offer
     */
    public function getOffer(): Offer
    {
        return $this->offer;
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function offer(): BelongsTo
    {
        return $this->belongsTo(Offer::class);
    }
}