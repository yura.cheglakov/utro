<?php

namespace App\Models\Tables;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Attachment
 *
 * @property string $id
 * @property string $attachmentable_type
 * @property int $attachmentable_id
 * @property string $mime
 * @property string $path
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Attachment newModelQuery()
 * @method static Builder|Attachment newQuery()
 * @method static Builder|Attachment query()
 * @method static Builder|Attachment whereAttachmentableId($value)
 * @method static Builder|Attachment whereAttachmentableType($value)
 * @method static Builder|Attachment whereCreatedAt($value)
 * @method static Builder|Attachment whereId($value)
 * @method static Builder|Attachment whereMime($value)
 * @method static Builder|Attachment wherePath($value)
 * @method static Builder|Attachment whereUpdatedAt($value)
 */
class Attachment extends Model
{
    use HasUuids;
}
