<?php

namespace App\Models\Tables;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;


/**
 * App\Models\Category
 *
 * @property int|null $id
 * @property int|null $external_id
 * @property boolean $is_display_on_main;
 * @property Collection<Category> $children
 * @property Collection<Product> $products
 * @property string $title
 * @property string $slug
 * @property bool $is_active
 * @property array|null $extra
 * @property int|null $category_id
 * @property Carbon|null $deleted_at
 * @property-read int|null $children_count
 * @property-read Category|null $parent
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static Builder|Category onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereExtra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereIsDisplayOnMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereTitle($value)
 * @method static Builder|Category withTrashed()
 * @method static Builder|Category withoutTrashed()
 */
class Category extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    protected $casts = [
        'extra' => 'array'
    ];

    protected $fillable = [
        'external_id',
        'is_active',
        'title',
        'slug'
    ];

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Category
     */
    public function setId(int $id): Category
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getExternalId(): ?int
    {
        return $this->external_id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return null|string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @return bool
     */
    public function isDisplayOnMain(): bool
    {
        return $this->is_display_on_main;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->is_active;
    }

    /**
     * @return array
     */
    public function getExtra(): array
    {
        return $this->extra;
    }

    public function clearRouteCache()
    {
        Cache::forget("category-route-$this->id");
    }

    public function getRoute(array $slugs = []): string|UrlGenerator|Application
    {
        return Cache::remember("category-route-$this->id", now()->addMonth(), function() use ($slugs) {
            $category = $this;

            do {
                $slugs = [$category->getSlug(), ...$slugs];
                $category = $category->parent;
            } while (null !== $category);

            return url(collect(["catalog", ...$slugs])->join("/"));
        });
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(self::class, 'category_id')->with('parent');
    }

    public function children(): HasMany
    {
        return $this->hasMany(self::class, 'category_id')->with('children');
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class);
    }
}
