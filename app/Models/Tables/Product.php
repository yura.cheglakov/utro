<?php

namespace App\Models\Tables;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * App\Models\Product
 *
 * @property-read int $id;
 * @property-read int $external_id
 * @property-read string $title
 * @property-read string $slug
 * @property-read null|string $description
 * @property-read bool $is_display_on_main
 * @property-read bool $is_new
 * @property-read bool $is_preorder
 * @property-read bool $is_hit
 * @property-read array $extra
 * @property-read Carbon|null $created_at
 * @property-read Carbon|null $updated_at
 * @property-read Carbon|null $deleted_at
 * @property-read Collection<Category> $categories
 * @property-read Collection<Offer> $offers
 * @property-read Collection<Product> $variants
 * @property-read bool $is_sale
 * @property-read int|null $categories_count
 * @property-read int|null $offers_count
 * @method static \Illuminate\Database\Eloquent\Builder|Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product newQuery()
 * @method static Builder|Product onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereExtra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereIsDisplayOnMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereIsHit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereIsNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereIsPreorder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereIsSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereUpdatedAt($value)
 * @method static Builder|Product withTrashed()
 * @method static Builder|Product withoutTrashed()
 * @property-read int|null $variants_count
 */
class Product extends Model
{
    use SoftDeletes;

    protected $attributes = [
        'is_display_on_main' => false,
        'is_new' => false,
        'is_hit' => false,
        'is_preorder' => false
    ];

    protected $fillable = [
        'external_id',
        'title',
        'description',
        'is_hit',
        'is_new',
        'slug'
    ];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getExternalId(): int
    {
        return $this->external_id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return null|string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return bool
     */
    public function isDisplayOnMain(): bool
    {
        return $this->is_display_on_main;
    }

    /**
     * @return bool
     */
    public function isNew(): bool
    {
        return $this->is_new;
    }

    /**
     * @return bool
     */
    public function isPreorder(): bool
    {
        return $this->is_preorder;
    }

    /**
     * @return bool
     */
    public function isHit(): bool
    {
        return $this->is_hit;
    }

    /**
     * @return array
     */
    public function getExtra(): array
    {
        return $this->extra;
    }

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return $this->created_at;
    }

    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updated_at;
    }

    /**
     * @return Carbon
     */
    public function getDeletedAt(): Carbon
    {
        return $this->deleted_at;
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }

    public function offers(): HasMany
    {
        return $this->hasMany(Offer::class);
    }

    public function variants(): BelongsToMany
    {
        return $this->belongsToMany(related: self::class, table: 'product_variant', relatedPivotKey: 'variant_id');
    }
}
