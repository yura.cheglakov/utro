<?php

namespace App\Models\Tables;

use App\Casts\OfferQuantityCast;
use App\Types\OfferQuantity;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;

/**
 * App\Models\Offer
 *
 * @property-read string $id
 * @property-read int $external_id
 * @property-read string $title
 * @property-read null|string $slug
 * @property-read string $article
 * @property-read bool $is_display_on_main
 * @property-read bool $is_new
 * @property-read bool $is_preorder
 * @property-read bool $is_hit
 * @property-read bool $is_active
 * @property-read bool $is_sale
 * @property-read OfferQuantity $quantity
 * @property-read array $extra
 * @property-read Carbon $crated_at
 * @property-read Carbon $updated_at
 * @property-read Carbon $deleted_at
 * @property-read Product $product
 * @property-read int $product_id
 * @property-read Carbon|null $created_at
 * @property-read Collection|OfferPrice[] $prices
 * @property-read Collection|PropertyValue[] $properties
 * @property-read int|null $properties_count
 * @method static \Illuminate\Database\Eloquent\Builder|Offer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Offer newQuery()
 * @method static Builder|Offer onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Offer query()
 * @method static \Illuminate\Database\Eloquent\Builder|Offer whereArticle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Offer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Offer whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Offer whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Offer whereExtra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Offer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Offer whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Offer whereIsDisplayOnMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Offer whereIsHit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Offer whereIsNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Offer whereIsPreorder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Offer whereIsSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Offer whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Offer whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Offer whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Offer whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Offer whereUpdatedAt($value)
 * @method static Builder|Offer withTrashed()
 * @method static Builder|Offer withoutTrashed()
 * @property-read int|null $prices_count
 */
class Offer extends Model
{
    use HasUuids;
    use SoftDeletes;

    protected $attributes = [
        'is_display_on_main' => false,
        'is_new' => false,
        'is_hit' => false,
        'is_preorder' => false
    ];

    protected $fillable = [
        'external_id',
        'title',
        'description',
        'article',
        'is_active',
        'is_new',
        'is_hit',
        'is_preorder',
        'is_display_on_main',
        'quantity',
        'slug',
        'extra'
    ];

    protected $casts = [
        'extra' => 'array',
        'quantity' => OfferQuantityCast::class
    ];

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getExternalId(): int
    {
        return $this->external_id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return null|string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getArticle(): string
    {
        return $this->article;
    }

    /**
     * @return bool
     */
    public function isDisplayOnMain(): bool
    {
        return $this->is_display_on_main;
    }

    /**
     * @return bool
     */
    public function isNew(): bool
    {
        return $this->is_new;
    }

    /**
     * @return bool
     */
    public function isPreorder(): bool
    {
        return $this->is_preorder;
    }

    /**
     * @return bool
     */
    public function isHit(): bool
    {
        return $this->is_hit;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->is_active;
    }

    /**
     * @return OfferQuantity
     */
    public function getQuantity(): OfferQuantity
    {
        return $this->quantity;
    }

    /**
     * @return array
     */
    public function getExtra(): array
    {
        return $this->extra;
    }

    /**
     * @return Carbon
     */
    public function getCratedAt(): Carbon
    {
        return $this->crated_at;
    }

    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updated_at;
    }

    /**
     * @return Carbon
     */
    public function getDeletedAt(): Carbon
    {
        return $this->deleted_at;
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function properties(): BelongsToMany
    {
        return $this->belongsToMany(PropertyValue::class);
    }

    public function prices(): HasMany
    {
        return $this->hasMany(OfferPrice::class);
    }

    /**
     * @return Collection<OfferPrice>|OfferPrice[]
     */
    public function getPrices(): Collection|array
    {
        return $this->prices;
    }
}
