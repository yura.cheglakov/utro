<?php

namespace App\Models\Tables;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProductOption
 *
 * @method static Builder|ProductOption newModelQuery()
 * @method static Builder|ProductOption newQuery()
 * @method static Builder|ProductOption query()
 */
class ProductOption extends Model
{
}
