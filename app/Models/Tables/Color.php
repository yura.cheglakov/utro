<?php

namespace App\Models\Tables;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Color
 *
 * @property int $id
 * @property string $title
 * @property string $hex
 * @method static Builder|Color newModelQuery()
 * @method static Builder|Color newQuery()
 * @method static Builder|Color query()
 * @method static Builder|Color whereHex($value)
 * @method static Builder|Color whereId($value)
 * @method static Builder|Color whereTitle($value)
 */
class Color extends Model
{
}
