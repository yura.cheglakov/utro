<?php

namespace App\Models\Tables;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Setting
 *
 * @property string $key
 * @property mixed $value
 * @method static Builder|Setting newModelQuery()
 * @method static Builder|Setting newQuery()
 * @method static Builder|Setting query()
 * @method static Builder|Setting whereKey($value)
 * @method static Builder|Setting whereValue($value)
 */
class Setting extends Model
{
    protected $primaryKey = 'code';
    protected $keyType = 'string';
    public $timestamps = false;
}
