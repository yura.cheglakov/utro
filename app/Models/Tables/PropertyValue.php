<?php

namespace App\Models\Tables;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\PropertyValue
 *
 * @property null|int $id
 * @property string $value
 * @property string $property_code
 * @property-read Property $property
 * @method static Builder|PropertyValue newModelQuery()
 * @method static Builder|PropertyValue newQuery()
 * @method static Builder|PropertyValue query()
 * @method static Builder|PropertyValue whereId($value)
 * @method static Builder|PropertyValue wherePropertyCode($value)
 * @method static Builder|PropertyValue whereValue($value)
 */
class PropertyValue extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'value',
        'property_code'
    ];

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPropertyCode(): string
    {
        return $this->property_code;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return PropertyValue
     */
    public function setValue(string $value): PropertyValue
    {
        $this->value = $value;
        return $this;
    }

    public function property(): BelongsTo
    {
        return $this->belongsTo(Property::class);
    }
}
