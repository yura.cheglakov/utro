<?php

namespace App\Models\Tables;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\OfferPrice
 *
 * @property int $id
 * @property string $offer_id
 * @property string $price_type_code
 * @property int $price
 * @property-read Offer $offer
 * @property-read PriceType $priceType
 * @method static Builder|OfferPrice newModelQuery()
 * @method static Builder|OfferPrice newQuery()
 * @method static Builder|OfferPrice query()
 * @method static Builder|OfferPrice whereId($value)
 * @method static Builder|OfferPrice whereOfferId($value)
 * @method static Builder|OfferPrice wherePrice($value)
 * @method static Builder|OfferPrice wherePriceTypeCode($value)
 */
class OfferPrice extends Model
{
    public $timestamps = false;

    protected $fillable = [
        "offer_id",
        "price_type_code",
        "price"
    ];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getOfferId(): string
    {
        return $this->offer_id;
    }

    /**
     * @return string
     */
    public function getPriceTypeCode(): string
    {
        return $this->price_type_code;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    public function offer(): BelongsTo
    {
        return $this->belongsTo(Offer::class);
    }

    public function priceType(): BelongsTo
    {
        return $this->belongsTo(PriceType::class);
    }
}
