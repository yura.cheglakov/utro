<?php

namespace App\Models\Tables;

use App\Casts\OrderStatusCast;
use App\Types\OrderStatus;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Order
 *
 * @property string $id
 * @property string $number
 * @property string|null $external_id
 * @property string $payment
 * @property string $delivery
 * @property string $buyer_first_name
 * @property string $buyer_last_name
 * @property string $buyer_middle_name
 * @property string $buyer_email
 * @property string $buyer_phone
 * @property string $buyer_address
 * @property int $amount
 * @property int $delivery_amount
 * @property int $cart_amount
 * @property int $discount_amount
 * @property string|null $payment_id
 * @property mixed $cart
 * @property mixed $extra
 * @property OrderStatus $status
 * @property string|null $coupon
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Order newModelQuery()
 * @method static Builder|Order newQuery()
 * @method static Builder|Order query()
 * @method static Builder|Order whereAmount($value)
 * @method static Builder|Order whereBuyerAddress($value)
 * @method static Builder|Order whereBuyerEmail($value)
 * @method static Builder|Order whereBuyerFirstName($value)
 * @method static Builder|Order whereBuyerLastName($value)
 * @method static Builder|Order whereBuyerMiddleName($value)
 * @method static Builder|Order whereBuyerPhone($value)
 * @method static Builder|Order whereCart($value)
 * @method static Builder|Order whereCartAmount($value)
 * @method static Builder|Order whereCoupon($value)
 * @method static Builder|Order whereCreatedAt($value)
 * @method static Builder|Order whereDelivery($value)
 * @method static Builder|Order whereDeliveryAmount($value)
 * @method static Builder|Order whereDiscountAmount($value)
 * @method static Builder|Order whereExternalId($value)
 * @method static Builder|Order whereExtra($value)
 * @method static Builder|Order whereId($value)
 * @method static Builder|Order whereNumber($value)
 * @method static Builder|Order wherePayment($value)
 * @method static Builder|Order wherePaymentId($value)
 * @method static Builder|Order whereStatus($value)
 * @method static Builder|Order whereUpdatedAt($value)
 */
class Order extends Model
{
    use HasUuids;

    protected $casts = [
        'status' => OrderStatusCast::class
    ];
}
