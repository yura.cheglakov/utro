<?php

namespace App\Models\Tables;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;

/**
 * App\Models\PriceType
 *
 * @property string $code
 * @property string $title
 * @property Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|PriceType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PriceType newQuery()
 * @method static Builder|PriceType onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|PriceType query()
 * @method static \Illuminate\Database\Eloquent\Builder|PriceType whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceType whereTitle($value)
 * @method static Builder|PriceType withTrashed()
 * @method static Builder|PriceType withoutTrashed()
 */
class PriceType extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'code';
    protected $keyType = 'string';
    public $timestamps = false;

    const BASE_PRICE_TYPE = "base";
    const SALES_PRICE_TYPE = "Sales";

    protected $fillable = [
        'code',
        'title'
    ];
}
