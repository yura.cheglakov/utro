<?php

namespace App\Models\Tables;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * App\Models\Menu
 *
 * @property Collection<MenuItem> $items
 * @property int $id
 * @property string $title
 * @property string $code
 * @property-read int|null $items_count
 * @method static Builder|Menu newModelQuery()
 * @method static Builder|Menu newQuery()
 * @method static Builder|Menu query()
 * @method static Builder|Menu whereCode($value)
 * @method static Builder|Menu whereId($value)
 * @method static Builder|Menu whereTitle($value)
 */
class Menu extends Model
{
    public $timestamps = false;

    public function items(): HasMany
    {
        return $this->hasMany(MenuItem::class);
    }
}
