<?php

namespace App\Models\Tables;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\Property
 *
 * @property string $code
 * @property string $title
 * @property boolean $visible
 * @property-read Collection|Category[] $categories
 * @property-read int|null $categories_count
 * @method static Builder|Property newModelQuery()
 * @method static Builder|Property newQuery()
 * @method static Builder|Property query()
 * @method static Builder|Property whereCode($value)
 * @method static Builder|Property whereTitle($value)
 * @method static Builder|Property whereVisible($value)
 */
class Property extends Model
{
    protected $primaryKey = 'code';
    protected $keyType = 'string';
    public $timestamps = false;

    protected $casts = [
        'visible' => 'boolean'
    ];

    protected $fillable = [
        'code',
        'title',
        'visible'
    ];

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Property
     */
    public function setCode(string $code): Property
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Property
     */
    public function setTitle(string $title): Property
    {
        $this->title = $title;
        return $this;
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->visible;
    }

    /**
     * @param bool $visible
     * @return Property
     */
    public function setIsVisible(bool $visible): Property
    {
        $this->visible = $visible;
        return $this;
    }
}
