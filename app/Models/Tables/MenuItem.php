<?php

namespace App\Models\Tables;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * App\Models\MenuItem
 *
 * @property string $title
 * @property string $link
 * @property Collection<MenuItem> $items
 * @property int $id
 * @property int $menu_id
 * @property int|null $menu_item_id
 * @property bool $display_banner
 * @property-read int|null $items_count
 * @method static Builder|MenuItem newModelQuery()
 * @method static Builder|MenuItem newQuery()
 * @method static Builder|MenuItem query()
 * @method static Builder|MenuItem whereDisplayBanner($value)
 * @method static Builder|MenuItem whereId($value)
 * @method static Builder|MenuItem whereLink($value)
 * @method static Builder|MenuItem whereMenuId($value)
 * @method static Builder|MenuItem whereMenuItemId($value)
 * @method static Builder|MenuItem whereTitle($value)
 */
class MenuItem extends Model
{
    public $timestamps = false;

    public function items(): HasMany
    {
        return $this->hasMany(MenuItem::class)->with('items');
    }
}
