<?php

namespace App\Models\Tables;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Status
 *
 * @property string $code
 * @property string $description
 * @property string $title
 * @method static Builder|Status newModelQuery()
 * @method static Builder|Status newQuery()
 * @method static Builder|Status query()
 * @method static Builder|Status whereCode($value)
 * @method static Builder|Status whereDescription($value)
 * @method static Builder|Status whereTitle($value)
 */
class Status extends Model
{
    protected $primaryKey = 'code';
    protected $keyType = 'string';
    public $timestamps = false;
}
