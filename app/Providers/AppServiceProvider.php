<?php

namespace App\Providers;

use App\Services\Cdek\Classes\AuthorizeRequestDto;
use App\Utilities\Delivery\Strategy\RussianPost\AbstractRussianPostStrategy;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->when(AuthorizeRequestDto::class)
            ->needs('$clientId')
            ->giveConfig('services.delivery.cdek.client');

        $this->app->when(AuthorizeRequestDto::class)
            ->needs('$clientSecret')
            ->giveConfig('services.delivery.cdek.secret');

        $this->app->singleton(AbstractRussianPostStrategy::class, function($app) {
            return $app->make(AbstractRussianPostStrategy::class);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }
}
