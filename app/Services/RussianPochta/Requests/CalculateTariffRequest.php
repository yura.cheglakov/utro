<?php

namespace App\Services\RussianPochta\Requests;

use App\Contracts\Delivery\DeliveryDto;
use App\Contracts\Delivery\DeliveryResponse;
use App\Services\RussianPochta\Exceptions\ExceededMaximumWeightException;
use App\Services\RussianPochta\Exceptions\RussianPochtaServiceException;
use App\Services\RussianPochta\Responses\CalculateTariffResponse;
use Illuminate\Support\Facades\Http;

class CalculateTariffRequest
{
    private const URI = "https://tariff.pochta.ru/v2/calculate/tariff/delivery";

    public function __construct(protected DeliveryDto $dto) {
    }

    /**
     * @throws ExceededMaximumWeightException
     * @throws RussianPochtaServiceException
     */
    public function send(): DeliveryResponse
    {
        return new CalculateTariffResponse(
            Http::timeout(30)
                ->get(self::URI, $this->dto->toCollection()->toArray())
        );
    }
}