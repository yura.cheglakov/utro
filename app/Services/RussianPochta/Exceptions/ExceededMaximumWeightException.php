<?php

namespace App\Services\RussianPochta\Exceptions;

use Exception;

class ExceededMaximumWeightException extends Exception
{
    protected $code = 2003;
}