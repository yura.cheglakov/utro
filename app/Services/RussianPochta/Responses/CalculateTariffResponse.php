<?php

namespace App\Services\RussianPochta\Responses;

use App\Contracts\Delivery\DeliveryResponse;
use App\Services\RussianPochta\Classes\DeliveryRange;
use App\Services\RussianPochta\Exceptions\ExceededMaximumWeightException;
use App\Services\RussianPochta\Exceptions\RussianPochtaServiceException;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;

class CalculateTariffResponse implements DeliveryResponse
{
    protected int $price;
    protected ?string $title;

    protected DeliveryRange $deliveryRange;

    /**
     * @throws ExceededMaximumWeightException
     * @throws RussianPochtaServiceException
     */
    public function __construct(PromiseInterface|Response $response)
    {
        $data = $response->json();


        if (200 === $response->status() && $data) {
            $this->price = Arr::get($data, 'paynds');
            $this->deliveryRange = new DeliveryRange(Arr::get($data, 'delivery.min'), Arr::get($data, 'delivery.max'));
        }

        if ($data && $errors = Arr::get($data, 'errors')) {
            foreach ($errors as $error) {
                if (2003 === (int) Arr::get($error, 'code')) {
                    throw new ExceededMaximumWeightException(Arr::get($error, 'msg'));
                }

                throw new RussianPochtaServiceException(Arr::get($error, 'msg'));
            }
        }
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return DeliveryRange
     */
    public function getDeliveryRange(): DeliveryRange
    {
        return $this->deliveryRange;
    }

    public function getTitle(): string
    {
        if (is_null($this->title)) {
            throw new \UnexpectedValueException("Необходимо задать название службы доставки");
        }

        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;
        return $this;
    }
}