<?php

namespace App\Services\RussianPochta\Classes;

use App\Contracts\Delivery\DeliveryDto;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class CalculateTariffDto implements DeliveryDto
{
    public function __construct(
        protected int $object,
        protected int $indexFrom,
        protected int $indexTo,
        protected int $weight,
        protected int $pack,
        protected ?int $declaredValue = null,
        protected ?array $service = null
    ) {
    }

    public function toCollection(): Collection
    {
        return collect([
            'object' => $this->object,
            'from' => $this->indexFrom,
            'to' => $this->indexTo,
            'weight' => $this->weight,
            'pack' => $this->pack
        ])
            ->when($this->declaredValue, fn(Collection $collection, int $value) => $collection->put('sumoc', $value))
            ->when(
                $this->service,
                fn(Collection $collection, array $value) => $collection->put('service', Arr::join($value, ","))
            );
    }
}