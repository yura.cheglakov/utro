<?php

namespace App\Services\RussianPochta\Classes;

class DeliveryRange implements \App\Contracts\Delivery\DeliveryRange
{
    public function __construct(
        protected int $daysMin,
        protected int $daysMax
    ) {
        if ($this->daysMax === $this->daysMin) {
            $this->daysMax += 3;
        }
    }

    /**
     * @return int
     */
    public function getDaysMin(): int
    {
        return $this->daysMin;
    }

    /**
     * @return int
     */
    public function getDaysMax(): int
    {
        return $this->daysMax;
    }
}