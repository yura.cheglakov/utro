<?php

namespace App\Services\RussianPochta;

use App\Contracts\Delivery\DeliveryDto;
use App\Contracts\Delivery\DeliveryResponse;
use App\Contracts\Delivery\DeliveryService;
use App\Services\RussianPochta\Exceptions\ExceededMaximumWeightException;
use App\Services\RussianPochta\Exceptions\RussianPochtaServiceException;
use App\Services\RussianPochta\Requests\CalculateTariffRequest;

class RussianPochtaService implements DeliveryService
{
    /**
     * @throws ExceededMaximumWeightException
     * @throws RussianPochtaServiceException
     */
    public function calculate(DeliveryDto $dto): DeliveryResponse
    {
        return (new CalculateTariffRequest($dto))->send();
    }
}