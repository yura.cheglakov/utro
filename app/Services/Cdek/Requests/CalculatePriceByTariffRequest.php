<?php

namespace App\Services\Cdek\Requests;

use App\Contracts\Delivery\DeliveryDto;
use App\Services\Cdek\Exceptions\UnauthorizedException;
use App\Services\Cdek\Responses\CalculatePriceByTariffResponse;
use Illuminate\Support\Facades\Http;

class CalculatePriceByTariffRequest
{
    protected ?string $uri = null;

    public function __construct(protected DeliveryDto $dto, protected string $token)
    {
        $this->uri = "https://api.cdek.ru/v2/calculator/tariff";

        if (config('app.debug') === true) {
            $this->uri = "https://api.edu.cdek.ru/v2/calculator/tariff";
        }
    }

    /**
     * @throws UnauthorizedException
     */
    public function send(): CalculatePriceByTariffResponse
    {
        return new CalculatePriceByTariffResponse(
            Http::timeout(30)
                ->asJson()
                ->withHeaders([
                    'Authorization' => "Bearer $this->token"
                ])
                ->post($this->uri, $this->dto->toCollection())
        );
    }
}