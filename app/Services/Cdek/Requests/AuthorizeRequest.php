<?php

namespace App\Services\Cdek\Requests;

use App\Services\Cdek\Classes\AuthorizeRequestDto;
use App\Services\Cdek\Exceptions\CdekServiceException;
use App\Services\Cdek\Responses\AuthorizeResponse;
use Illuminate\Support\Facades\Http;

class AuthorizeRequest
{
    protected string $uri = "https://api.cdek.ru/v2/oauth/token";

    public function __construct(protected AuthorizeRequestDto $dto)
    {
    }

    /**
     * @throws CdekServiceException
     */
    public function send(): AuthorizeResponse
    {
        return new AuthorizeResponse(
            Http::timeout(30)->asForm()->post($this->uri, $this->dto->toCollection()->toArray())
        );
    }
}