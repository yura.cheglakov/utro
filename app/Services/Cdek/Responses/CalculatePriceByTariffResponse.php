<?php

namespace App\Services\Cdek\Responses;

use App\Contracts\Delivery\DeliveryRange;
use App\Contracts\Delivery\DeliveryResponse;
use App\Services\Cdek\Exceptions\UnauthorizedException;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Http\Client\Response;

class CalculatePriceByTariffResponse implements DeliveryResponse
{
    protected ?string $title = null;

    /**
     * @throws UnauthorizedException
     */
    public function __construct(PromiseInterface|Response $response)
    {
        if (401 === $response->status()) {
            throw new UnauthorizedException("Получен код 401");
        }
    }

    public function getPrice(): int
    {
        return 100;
    }

    public function getDeliveryRange(): DeliveryRange
    {
        return new \App\Services\Cdek\Classes\DeliveryRange(3, 4);
    }

    public function getTitle(): string
    {
        if (is_null($this->title)) {
            throw new \UnexpectedValueException("Необходимо задать название службы доставки");
        }
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;
        return $this;
    }
}