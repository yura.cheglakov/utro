<?php

namespace App\Services\Cdek\Responses;

use App\Services\Cdek\Exceptions\CdekServiceException;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;

class AuthorizeResponse
{
    protected string $accessToken;

    /**
     * @throws CdekServiceException
     */
    public function __construct(PromiseInterface|Response $response)
    {
        if (200 !== $response->status()) {
            throw new CdekServiceException("Ошибка");
        }

        $data = $response->json();
        $token = Arr::get($data, 'access_token');

        $this->accessToken = $token;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }
}