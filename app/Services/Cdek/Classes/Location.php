<?php

namespace App\Services\Cdek\Classes;

use Illuminate\Support\Collection;

class Location
{
    public function __construct(
        protected ?int $code = null,
        protected ?int $postalCode = null,
        protected ?string $countryCode = null,
        protected ?string $city = null,
        protected ?string $address = null,
    ) {
        if (!($this->code || $this->postalCode || $this->address)) {
            throw new \InvalidArgumentException("Необходимо указать либо CDEK код города, либо почтовый индекс, либо полный адрес");
        }
    }

    public function toArray(): array
    {
        return collect()
            ->when($this->code, fn(Collection $collection, $value) => $collection->put('code', $value))
            ->when($this->postalCode, fn(Collection $collection, $value) => $collection->put('postal_code', $value))
            ->when($this->countryCode, fn(Collection $collection, $value) => $collection->put('country_code', $value))
            ->when($this->city, fn(Collection $collection, $value) => $collection->put('city', $value))
            ->when($this->address, fn(Collection $collection, $value) => $collection->put('address', $value))
            ->toArray();
    }
}