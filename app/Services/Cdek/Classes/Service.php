<?php

namespace App\Services\Cdek\Classes;

class Service
{
    public function __construct(
        protected string $code
    ) {
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }
}