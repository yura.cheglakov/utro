<?php

namespace App\Services\Cdek\Classes;

use Illuminate\Support\Collection;

class AuthorizeRequestDto
{
    public function __construct(
        protected string $clientId,
        protected string $clientSecret,
        protected string $grantType = 'client_credentials',
    ) {
    }

    public function toCollection(): Collection
    {
        return collect([
            'grant_type' => $this->grantType,
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret
        ]);
    }
}