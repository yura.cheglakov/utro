<?php

namespace App\Services\Cdek\Classes;

use App\Contracts\Delivery\DeliveryDto;
use Illuminate\Support\Collection;

class CalculateByTariffDto implements DeliveryDto
{
    public function __construct(
        protected PlannedTransferDate $plannedTransferDate,
        protected int $tariffCode,
        protected Location $toLocation,
        protected Location $fromLocation,
        protected Collection $packages,
        protected int $type = 1,
        protected int $currency = 1,
        protected ?Collection $services = null,
    ) {
        if (null === $this->services) {
            $this->services = collect();
        }
    }

    public function toCollection(): Collection
    {
        return collect([
            'type' => $this->type,
            'currency' => $this->currency,
            'tariff_code' => $this->tariffCode,
            'from_location' => $this->fromLocation->toArray(),
            'to_location' => $this->toLocation->toArray(),
            'services' => $this->services->mapWithKeys(fn(Service $service) => ['code' => $service->getCode()])->toArray(),
            'packages' => $this->packages->map(fn(Package $package) => $package->toArray())
        ]);
    }
}