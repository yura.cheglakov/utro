<?php

namespace App\Services\Cdek\Classes;

use Illuminate\Support\Carbon;

/**
 * Планируемая дата передачи заказа в службу доставки
 */
class PlannedTransferDate
{
    protected Carbon $value;

    public function __construct(Carbon $value)
    {
        if (now()->endOfDay() > $value) {
            throw new \UnexpectedValueException("Дата планируемой доставки не может быть раньше конца текущего дня");
        }

        $this->value = $value;
    }

    /**
     * @return Carbon
     */
    public function getValue(): Carbon
    {
        return $this->value;
    }
}