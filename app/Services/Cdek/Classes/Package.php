<?php

namespace App\Services\Cdek\Classes;

use App\Types\UnsignedInteger;
use App\Types\UnsignedNotZeroInteger;
use Illuminate\Support\Collection;

class Package
{

    /**
     * @param UnsignedNotZeroInteger $weight Вес в граммах
     * @param UnsignedInteger $length Длина в миллиметрах
     * @param UnsignedInteger $width Ширина в миллиметрах
     * @param UnsignedInteger|null $height Высота в миллиметрах
     */
    public function __construct(
        protected UnsignedNotZeroInteger $weight,
        protected UnsignedInteger $length,
        protected UnsignedInteger $width,
        protected ?UnsignedInteger $height = null
    ) {
    }

    public function toArray(): array
    {
        return collect([
            'weight' => $this->weight->value(),
        ])
            ->when($this->length->value(), fn(Collection $collection, $value) => $collection->put('length', $value / 10))
            ->when($this->width->value(), fn(Collection $collection, $value) => $collection->put('width', $value / 10))
            ->when($this->height?->value(), fn(Collection $collection, $value) => $collection->put('height', $value / 10))
            ->toArray();
    }
}