<?php

namespace App\Services\Cdek;

use App\Contracts\Delivery\DeliveryDto;
use App\Contracts\Delivery\DeliveryResponse;
use App\Contracts\Delivery\DeliveryService;
use App\Services\Cdek\Classes\AuthorizeRequestDto;
use App\Services\Cdek\Exceptions\UnauthorizedException;
use App\Services\Cdek\Requests\AuthorizeRequest;
use App\Services\Cdek\Requests\CalculatePriceByTariffRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;

class CdekService implements DeliveryService
{
    protected string $token;

    public function __construct()
    {
        $this->token = $this->authorize();
    }

    public function authorize(): string
    {
        return Cache::remember('cdek-token', now()->addSeconds(3500), function() {
           return (new AuthorizeRequest(App::make(AuthorizeRequestDto::class)))->send()->getAccessToken();
        });
    }

    /**
     * @throws UnauthorizedException
     */
    public function calculate(DeliveryDto $dto): DeliveryResponse
    {
        return (new CalculatePriceByTariffRequest($dto, $this->token))->send();
    }
}