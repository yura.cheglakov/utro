<?php

namespace App\Services\RetailCrm\Traits;

use App\Services\RetailCrm\Requests\ProductsRequest;

/**
 * @property int $limit
 * @property int $page
 */
trait PageLimitSettersTrait
{
    /**
     * @param int $limit
     * @return static
     */
    public function setLimit(int $limit): static
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @param int $page
     * @return static
     */
    public function setPage(int $page): static
    {
        $this->page = $page;
        return $this;
    }

    public function incrementPage(): static
    {
        if (!$this->page) {
            $this->page = 1;
            return $this;
        }

        $this->page += 1;
        return $this;
    }
}
