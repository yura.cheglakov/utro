<?php

namespace App\Services\RetailCrm\Providers;

use App\Services\RetailCrm\RetailCrmService;
use Illuminate\Support\ServiceProvider;

class RetailCrmServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->when(RetailCrmService::class)
            ->needs('$apiUrl')
            ->giveConfig('services.retail.api.url');

        $this->app->when(RetailCrmService::class)
            ->needs('$apiKey')
            ->giveConfig('services.retail.api.key');

        $this->app->when(RetailCrmService::class)
            ->needs('$paginationLimit')
            ->giveConfig('services.retail.pagination_limit');
    }
}
