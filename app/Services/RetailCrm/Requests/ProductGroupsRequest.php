<?php

namespace App\Services\RetailCrm\Requests;

use App\Services\RetailCrm\Traits\PageLimitSettersTrait;
use RetailCrm\Api\Model\Filter\Store\ProductGroupFilterType;

class ProductGroupsRequest extends \RetailCrm\Api\Model\Request\Store\ProductGroupsRequest
{
    use PageLimitSettersTrait;

    /**
     * @param ProductGroupFilterType $filter
     * @return ProductGroupsRequest
     */
    public function setFilter(ProductGroupFilterType $filter): ProductGroupsRequest
    {
        $this->filter = $filter;
        return $this;
    }
}
