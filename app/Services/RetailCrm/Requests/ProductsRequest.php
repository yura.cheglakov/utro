<?php

namespace App\Services\RetailCrm\Requests;

use App\Services\RetailCrm\Traits\PageLimitSettersTrait;
use RetailCrm\Api\Model\Filter\Store\ProductFilterType;

class ProductsRequest extends \RetailCrm\Api\Model\Request\Store\ProductsRequest
{
    use PageLimitSettersTrait;

    /**
     * @param ProductFilterType $filter
     * @return ProductsRequest
     */
    public function setFilter(ProductFilterType $filter): ProductsRequest
    {
        $this->filter = $filter;
        return $this;
    }
}
