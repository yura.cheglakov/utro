<?php

namespace App\Services\RetailCrm\Requests;

use App\Services\RetailCrm\Traits\PageLimitSettersTrait;
use RetailCrm\Api\Model\Filter\Store\ProductPropertiesFilterType;

class ProductPropertiesRequest extends \RetailCrm\Api\Model\Request\Store\ProductPropertiesRequest
{
    use PageLimitSettersTrait;

    public $page = 0;

    /**
     * @param ProductPropertiesFilterType $filter
     * @return ProductPropertiesRequest
     */
    public function setFilter(ProductPropertiesFilterType $filter): static
    {
        $this->filter = $filter;
        return $this;
    }
}
