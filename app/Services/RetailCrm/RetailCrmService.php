<?php

namespace App\Services\RetailCrm;

use App\Services\RetailCrm\Exceptions\RetailCrmServiceException;
use App\Services\RetailCrm\Requests\ProductGroupsRequest;
use App\Services\RetailCrm\Requests\ProductPropertiesRequest;
use App\Services\RetailCrm\Requests\ProductsRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use RetailCrm\Api\Client;
use RetailCrm\Api\Exception\Client\BuilderException;
use RetailCrm\Api\Factory\SimpleClientFactory;
use RetailCrm\Api\Model\Entity\References\PriceType;
use RetailCrm\Api\Model\Entity\Store\Product;
use RetailCrm\Api\Model\Entity\Store\ProductGroup;
use RetailCrm\Api\Model\Entity\Store\ProductProperty;
use Throwable;

class RetailCrmService
{
    protected Client $client;

    /**
     * @throws BuilderException
     */
    public function __construct(
        protected string $apiUrl,
        protected string $apiKey,
        protected int    $paginationLimit
    ) {
        $this->client = SimpleClientFactory::createClient($this->apiUrl, $this->apiKey);
    }

    /**
     * @param array<ProductProperty> $properties
     * @return array<ProductProperty>
     * @throws RetailCrmServiceException
     */
    public function getProductsProperties(array $properties = []): array
    {
        $request = (new ProductPropertiesRequest())
            ->setLimit($this->paginationLimit);

        do {
            try {
                $response = $this->client->store->productsProperties($request);
            } catch (Throwable $exception) {
                throw new RetailCrmServiceException($exception->getMessage());
            }

            $properties += $response->properties;
        } while ($response->pagination->currentPage !== $response->pagination->totalPageCount);

        return $properties;
    }

    /**
     * @param array<Product> $products
     * @return array<Product>
     * @throws RetailCrmServiceException
     */
    public function getProducts(array $products = []): array
    {
        $request = (new ProductsRequest())
            ->setLimit($this->paginationLimit);

        do {
            try {
                $response = $this->client->store->products($request->incrementPage());
            } catch (Throwable $exception) {
                throw new RetailCrmServiceException($exception->getMessage());
            }

            $products = array_merge($products, $response->products);
        } while ($response->pagination->currentPage < $response->pagination->totalPageCount);

        return $products;
    }

    /**
     * @param array<ProductGroup> $categories
     * @return array<ProductGroup>
     * @throws RetailCrmServiceException
     */
    public function getCategories(array $categories = []): array
    {
        $request = (new ProductGroupsRequest())
            ->setLimit($this->paginationLimit);

        do {
            try {
                $response = $this->client->store->productGroups($request->incrementPage());
            } catch (Throwable $exception) {
                throw new RetailCrmServiceException($exception->getMessage());
            }

            $categories += $response->productGroup;
        } while ($response->pagination->currentPage !== $response->pagination->totalPageCount);

        return $categories;
    }

    /**
     * @return array<PriceType>
     * @throws RetailCrmServiceException
     */
    public function getPriceTypes(): array
    {
        try {
            $response = $this->client->references->priceTypes();
        } catch (Throwable $exception) {
            throw new RetailCrmServiceException($exception->getMessage());
        }

        return $response->priceTypes;
    }
}
