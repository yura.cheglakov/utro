<?php

namespace App\Observers;

use App\Models\Tables\Category;

class CategoryObserver
{
    public function saving(Category $category): void
    {
        if ($category->isDirty('title') && empty($category->getSlug())) {
            $category->fill(['slug' => str($category->getTitle())->slug()]);
        }
    }

    public function updated(Category $category): void
    {
        if ($category->wasChanged('slug')) {
            $category->clearRouteCache();
        }
    }
}
