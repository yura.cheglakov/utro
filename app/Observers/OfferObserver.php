<?php

namespace App\Observers;

use App\Models\Tables\Offer;

class OfferObserver
{
    public function saving(Offer $offer): void
    {
        if ($offer->isDirty('title') && empty($offer->getSlug())) {
            $offer->fill(['slug' => str($offer->getTitle())->slug()->append('-')->append($offer->getArticle())]);
        }
    }
}
