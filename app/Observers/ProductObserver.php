<?php

namespace App\Observers;

use App\Models\Tables\Product;

class ProductObserver
{
    public function saving(Product $product): void
    {
        if ($product->isDirty('title') && empty($product->getSlug())) {
            $product->fill(['slug' => str($product->getTitle())->slug()]);
        }
    }
}
