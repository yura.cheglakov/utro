<?php

namespace App\Http\Resources;

use App\Models\Tables\Menu;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;


/**
 * @mixin Menu
 */
class MenuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'items' => MenuItemResource::collection($this->items()->with('items')->whereNull('menu_item_id')->get())
        ];
    }
}
