<?php

namespace App\Http\Resources;

use App\Models\Tables\MenuItem;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Spatie\Url\Url;


/**
 * @mixin MenuItem
 */
class MenuItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'title' => $this->title,
            'link' => (string) Url::fromString($this->link)->withHost('localhost:3000'),
            $this->mergeWhen(0 < $this->items->count(), function() {
                return [
                    'items' => MenuItemResource::collection($this->items)
                ];
            })
        ];
    }
}
