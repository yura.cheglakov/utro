<?php

namespace App\Http\Resources\Offer;

use App\Models\Tables\OfferPrice;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;


/**
 * @mixin OfferPrice
 */
class OfferPriceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'code' => str($this->getPriceTypeCode())->lower(),
            'price' => $this->getPrice()
        ];
    }
}
