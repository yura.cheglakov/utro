<?php

namespace App\Http\Resources;

use App\Models\Tables\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Spatie\Url\Url;


/**
 * @mixin Category
 */
class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'link' => (string) Url::fromString($this->getRoute())->withHost('localhost:3000'),
            $this->mergeWhen(0 < $this->children->count(), function() {
                return [
                    'children' => self::collection($this->children)
                ];
            })
        ];
    }
}
