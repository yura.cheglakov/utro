<?php

namespace App\Http\Resources\Catalog;

use App\Dto\CatalogItem;
use App\Http\Resources\Offer\OfferPriceResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Spatie\Url\Url;


/**
 * @mixin CatalogItem
 */
class CatalogItemResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'prices' => OfferPriceResource::collection($this->getPrices()),
            'link' => (string) Url::fromString(url($this->getLink()))->withHost("localhost:3000")
        ];
    }
}