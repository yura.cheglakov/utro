<?php

namespace App\Http\Controllers;

use App\Models\Tables\Category;
use App\Utilities\Catalog;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CatalogController extends Controller
{
    public function list(Category $category = null): AnonymousResourceCollection
    {
       return Catalog::getOffers($category);
    }
}
