<?php

namespace App\Http\Controllers;

use App\Http\Resources\MenuResource;
use App\Models\Tables\Menu;

class MenusController extends Controller
{
    public function get(Menu $menu): MenuResource
    {
        return new MenuResource($menu);
    }
}
