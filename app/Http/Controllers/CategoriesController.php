<?php

namespace App\Http\Controllers;

use App\Http\Resources\CategoryResource;
use App\Utilities\Category;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CategoriesController extends Controller
{
    public function list(): AnonymousResourceCollection
    {
        return CategoryResource::collection(Category::getCategoriesTree());
    }
}
