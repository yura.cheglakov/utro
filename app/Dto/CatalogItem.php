<?php

namespace App\Dto;

use App\Models\Tables\OfferPrice;
use Illuminate\Support\Collection;

class CatalogItem
{
    const PER_PAGE = 12;

    public function __construct(
        protected string     $id,
        protected string     $title,
        protected string     $link,
        protected Collection $prices,
    ) {
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return Collection<OfferPrice>
     */
    public function getPrices(): Collection
    {
        return $this->prices;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }
}