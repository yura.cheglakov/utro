<?php

namespace App\Utilities\Delivery\Classes;

class Address implements \App\Contracts\Address
{
    public function __construct(
        protected ?string $index,
        protected ?string $city
    ) {}

    /**
     * @return string|null
     */
    public function getIndex(): ?string
    {
        return $this->index;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }
}