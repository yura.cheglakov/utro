<?php

namespace App\Utilities\Delivery\Classes;

class Bound implements \App\Contracts\Bound
{
    /**
     * Параметры передаются в миллиметрах
     * @param int $width Ширина в миллиметрах
     * @param int $length Длина в миллиметрах
     * @param int $height Высота в миллиметрах
     */
    public function __construct(
        protected int $width,
        protected int $length,
        protected int $height = 0,
    ) {
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @return int
     */
    public function getLength(): int
    {
        return $this->length;
    }

    /**
     * Получить объем
     * @return int
     */
    public function getVolume(): int
    {
        if ($this->height) {
            return $this->height * $this->length * $this->width;
        }

        return $this->width * $this->length;
    }
}