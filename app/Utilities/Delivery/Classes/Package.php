<?php

namespace App\Utilities\Delivery\Classes;

use App\Contracts\Delivery\DeliveryPackage;

class Package implements DeliveryPackage
{
    /**
     * @param Address $address
     * @param Bound $bound
     * @param int $weight Вес в граммах
     * @param int $itemsCount Количество предметов для доставки
     * @param int|null $declaredAmount Объявленная ценность доставки в копейках
     */
    public function __construct(
        protected Address $address,
        protected Bound $bound,
        protected int $weight,
        protected int $itemsCount,
        protected ?int $declaredAmount,
    ) {
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * @return Bound
     */
    public function getBound(): Bound
    {
        return $this->bound;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @return int
     */
    public function getItemsCount(): int
    {
        return $this->itemsCount;
    }

    /**
     * @return int|null
     */
    public function getDeclaredAmount(): ?int
    {
        return $this->declaredAmount;
    }
}