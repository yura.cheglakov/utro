<?php

namespace App\Utilities\Delivery;

use App\Contracts\Delivery\DeliveryPackage;
use App\Contracts\Delivery\DeliveryStrategy;
use App\Utilities\Delivery\Strategy\RussianPost\RussianPostCourierStrategy;
use App\Utilities\Delivery\Strategy\RussianPost\RussianPostPvzStrategy;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;

class Delivery implements \App\Contracts\Delivery
{
    private const MAP = [
        'pochta' => RussianPostPvzStrategy::class,
        'pochta_courier' => RussianPostCourierStrategy::class
    ];

    /**
     * @param DeliveryPackage $package
     * @return Collection
     */
    public function getAllAvailableDeliveryVariants(DeliveryPackage $package): Collection
    {
        return collect(self::MAP)->mapWithKeys(function (string $class, string $key) use ($package) {
            /** @var DeliveryStrategy $strategy */
            $strategy = App::make($class);

            return [$key => $strategy->calculate($package)->setTitle($strategy->getTitle())];
        });
    }

    public function getMinimumPrice(DeliveryPackage $package)
    {
        // TODO: Implement getMinimumPrice() method.
    }
}