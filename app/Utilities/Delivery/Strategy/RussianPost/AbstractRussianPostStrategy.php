<?php

namespace App\Utilities\Delivery\Strategy\RussianPost;

use App\Contracts\Delivery\DeliveryResponse;
use App\Contracts\Delivery\DeliveryStrategy;
use App\Services\RussianPochta\Classes\CalculateTariffDto;
use App\Services\RussianPochta\Exceptions\ExceededMaximumWeightException;
use App\Services\RussianPochta\Exceptions\RussianPochtaServiceException;
use App\Services\RussianPochta\RussianPochtaService;
use App\Utilities\Delivery\Classes\Package;

abstract class AbstractRussianPostStrategy implements DeliveryStrategy
{
    public function __construct(protected RussianPochtaService $service)
    {
    }

    protected int $fromIndex = 460000;
    protected int $pack = 10;

    /**
     * @throws ExceededMaximumWeightException
     * @throws RussianPochtaServiceException
     */
    public function calculate(Package $package): DeliveryResponse
    {
        return $this->service->calculate(
            new CalculateTariffDto(
                $this->getObjectId(),
                $this->fromIndex,
                $package->getAddress()->getIndex(),
                $package->getWeight(),
                $this->pack,
                $package->getDeclaredAmount(),
            )
        );
    }

    abstract public function getTitle(): string;

    abstract protected function getObjectId(): int;
}