<?php

namespace App\Utilities\Delivery\Strategy\RussianPost;

class RussianPostPvzStrategy extends AbstractRussianPostStrategy
{
    public function getTitle(): string
    {
        return "Почта России";
    }

    protected function getObjectId(): int
    {
        return 23020;
    }
}