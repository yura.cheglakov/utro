<?php

namespace App\Utilities\Delivery\Strategy\RussianPost;

class RussianPostCourierStrategy extends AbstractRussianPostStrategy
{
    public function getTitle(): string
    {
        return "Почта России, Курьер";
    }

    protected function getObjectId(): int
    {
        return 24020;
    }
}