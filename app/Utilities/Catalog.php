<?php

namespace App\Utilities;

use App\Dto\CatalogItem;
use App\Http\Resources\Catalog\CatalogItemResource;
use App\Models\Views\CatalogOffer;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class Catalog
{
    public static function getOffers(?\App\Models\Tables\Category $category = null): AnonymousResourceCollection
    {
        $offers = self::getCatalogItems($category);
        $page = Paginator::resolveCurrentPage();

        return CatalogItemResource::collection(new LengthAwarePaginator(
            $offers->slice(($page - 1) * CatalogItem::PER_PAGE, CatalogItem::PER_PAGE),
            $offers->count(),
            CatalogItem::PER_PAGE,
            $page
        ));
    }

    protected static function getCatalogItems(\App\Models\Tables\Category $category = null)
    {
        if (null === $category) {
            return CatalogOffer::all()->map(function (CatalogOffer $item) {
                return (new CatalogItem(
                    $item->getOfferId(),
                    $item->getOffer()->getTitle(),
                    url(collect(['products', $item->getProduct()->getSlug(), $item->getOffer()->getSlug()])->join("/")),
                    $item->getOffer()->getPrices(),
                ));
            });
        }

        return CatalogItemResource::collection(
            CatalogOffer::query()
                ->whereIn('product_id', Category::extractProducts($category, collect())->pluck('id'))
                ->get()
                ->map(function (CatalogOffer $item) {
                    return (new CatalogItem(
                        $item->getOfferId(),
                        $item->getOffer()->getTitle(),
                        url(collect(['products', $item->getProduct()->getSlug(), $item->getOffer()->getSlug()])->join("/")),
                        $item->getOffer()->getPrices(),
                    ));
                })
        );
    }
}