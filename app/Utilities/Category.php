<?php

namespace App\Utilities;

use App\Models\Tables\Product;
use Illuminate\Support\Collection;

class Category
{
    public static function extractProducts(\App\Models\Tables\Category $category, Collection $products)
    {
        $products = $products->merge($category->products);

        foreach ($category->children as $child) {
            $products = $products->merge($child->products);

            if (0 < $child->children->count()) {
                $products = $products->merge(self::extractProducts($child, $products));
            }
        }

        return $products->unique(fn(Product $product) => $product->getId());
    }

    public static function getCategoriesTree(): \Illuminate\Database\Eloquent\Collection
    {
        return \App\Models\Tables\Category::with('children')->whereNull('category_id')->get();
    }
}