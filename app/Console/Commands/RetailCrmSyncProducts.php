<?php

namespace App\Console\Commands;

use App\Constants\Database\PostgreSqlError;
use App\Models\Tables\Category;
use App\Models\Tables\Offer;
use App\Models\Tables\OfferPrice;
use App\Models\Tables\Product;
use App\Models\Tables\Property;
use App\Models\Tables\PropertyValue;
use App\Services\RetailCrm\Exceptions\RetailCrmServiceException;
use App\Services\RetailCrm\RetailCrmService;
use App\Types\OfferQuantity;
use Illuminate\Console\Command;
use RetailCrm\Api\Model\Entity\FixExternalRow;
use Throwable;

class RetailCrmSyncProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retail:products:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Синхронизация продуктов из Retail CRM';

    public function __construct(protected RetailCrmService $retailCRMService)
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        if (!Category::query()->whereNotNull('external_id')->exists()) {
            $this->error("Нет категорий из RetailCRM. Не с чем синхронизировать!");
            return self::FAILURE;
        }

        if (!Property::query()->exists()) {
            $this->error("Нет аттрибутов из RetailCRM! Сначала запустите синхронизацию аттрибутов: php artisan retail:properties:sync");
            return self::FAILURE;
        }

        try {
            $retailProducts = $this->retailCRMService->getProducts();
        } catch (RetailCrmServiceException $e) {
            $this->error($e->getMessage());
            return self::FAILURE;
        }

        foreach ($retailProducts as $retailProduct) {
            /** @var Product $product */
            $product = Product::query()->firstOrNew(['external_id' => $retailProduct->id]);

            $product
                ->fill([
                    'title' => $retailProduct->name,
                    'description' => $retailProduct->description,
                    'is_hit' => !!$retailProduct->popular,
                    'is_new' => !!$retailProduct->novelty
                ]);

            try {
                $product->save();
            } catch (Throwable $exception) {
                if (PostgreSqlError::UNIQUE_VIOLATION == $exception->getCode()) {
                    $this->error("Повторяющийся slug в продукта. Продукт: {$product->getTitle()}");
                    continue;
                } else {
                    $this->error("Непредвиденная ошибка: {$exception->getMessage()}");
                    return self::FAILURE;
                }
            }

            $productRetailGroupsIds = collect($retailProduct->groups)->map(fn(FixExternalRow $group) => $group->id);
            $innerCategoriesIds = Category::query()->whereIn('external_id', $productRetailGroupsIds)->pluck('id');

            $product->categories()->sync($innerCategoriesIds);
            $retailProductOffers = $retailProduct->offers;

            foreach ($retailProductOffers as $retailProductOffer) {
                /** @var Offer $offer */
                $offer = Offer::query()->firstOrNew(['external_id' => $retailProductOffer->id]);

                if (empty($retailProductOffer->article)) {
                    $this->error("$retailProductOffer->id|$retailProductOffer->name имеет пустой артикул! Пропускаем... ");
                    continue;
                }

                $offer->fill([
                    'external_id' => $retailProductOffer->id,
                    'title' => $retailProductOffer->name,
                    'article' => $retailProductOffer->article,
                    'is_active' => !!$retailProductOffer->active,
                    'is_new' => $product->isNew(),
                    'is_hit' => $product->isHit(),
                    'is_preorder' => $product->isPreorder(),
                    'is_display_on_main' => $product->isDisplayOnMain(),
                    'quantity' => new OfferQuantity((int) $retailProductOffer->quantity)
                ]);

                $offer->product()->associate($product);

                $extra = [
                    "weight" => $retailProductOffer->weight,
                    "length" => $retailProductOffer->length,
                    "width" => $retailProductOffer->width,
                    "height" => $retailProductOffer->height,
                ];

                $offer->fill(['extra' => $extra]);

                try {
                    $offer->save();
                } catch (Throwable $exception) {
                    if (PostgreSqlError::UNIQUE_VIOLATION == $exception->getCode()) {
                        $this->error("Повторяющийся slug в рамках одного продукта. Продукт: {$product->getTitle()}, торговое предложение: {$offer->getTitle()}");
                        continue;
                    } else {
                        $this->error("Непредвиденная ошибка: {$exception->getMessage()}");
                        return self::FAILURE;
                    }
                }

                $offerPropertyValues = [];
                if ($retailProductOffer->properties) {
                    foreach ($retailProductOffer->properties as $retailProductOfferPropertyKey => $retailProductOfferPropertyValue) {
                        /** @var PropertyValue $propertyValue */
                        $propertyValue = PropertyValue::query()->firstOrCreate(['value' => $retailProductOfferPropertyValue, 'property_code' => $retailProductOfferPropertyKey]);

                        $offerPropertyValues[] = $propertyValue->getId();
                    }

                    $offer->properties()->sync($offerPropertyValues);
                }

                foreach ($retailProductOffer->prices as $retailProductOfferPrice) {
                    OfferPrice::query()
                        ->firstOrNew([
                            'offer_id' => $offer->getId(),
                            'price_type_code' => $retailProductOfferPrice->priceType,
                        ], ['price' => (int)$retailProductOfferPrice->price * 100])
                        ->save();
                }
            }
        }

        return self::SUCCESS;
    }
}
