<?php

namespace App\Console\Commands;

use App\Models\Tables\Category;
use App\Services\RetailCrm\Exceptions\RetailCrmServiceException;
use App\Services\RetailCrm\RetailCrmService;
use Illuminate\Console\Command;

class RetailCrmSyncCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retail:categories:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Синхронизация категорий с Retail CRM';

    public function __construct(protected RetailCrmService $retailCRMService)
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $categories = [];

        try {
            $retailCategories = $this->retailCRMService->getCategories();
        } catch (RetailCrmServiceException $e) {
            $this->error($e->getMessage());
            return self::FAILURE;
        }

        foreach ($retailCategories as $retailCategory) {
            /** @var Category $category */
            $category = Category::query()->firstOrNew([
                'external_id' => $retailCategory->id
            ], [
                'is_active' => $retailCategory->active,
                'title' => $retailCategory->name
            ]);

            $categories[$retailCategory->id] = $category;

            if ($retailCategory->parentId) {
                $category->parent()->associate($categories[$retailCategory->parentId]);
            }

            $category->save();
        }

        return self::SUCCESS;
    }
}
