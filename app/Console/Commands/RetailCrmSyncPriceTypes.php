<?php

namespace App\Console\Commands;

use App\Models\Tables\PriceType;
use App\Services\RetailCrm\Exceptions\RetailCrmServiceException;
use App\Services\RetailCrm\RetailCrmService;
use Illuminate\Console\Command;

class RetailCrmSyncPriceTypes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retail:prices:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Синхронизация типов стоимости из Retail CRM';

    public function __construct(protected RetailCrmService $retailCRMService)
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        try {
            $retailPriceTypes = $this->retailCRMService->getPriceTypes();
        } catch (RetailCrmServiceException $exception) {
            $this->error($exception->getMessage());
            return self::FAILURE;
        }

        foreach ($retailPriceTypes as $retailPriceType) {
            $priceType = PriceType::query()->firstOrNew([
                'code' => $retailPriceType->code
            ], ['title' => $retailPriceType->name]);

            $priceType->save();
        }

        return self::SUCCESS;
    }
}
