<?php

namespace App\Console\Commands;

use App\Models\Tables\Category;
use App\Models\Tables\Property;
use App\Services\RetailCrm\Exceptions\RetailCrmServiceException;
use App\Services\RetailCrm\RetailCrmService;
use Illuminate\Console\Command;
use RetailCrm\Api\Model\Entity\Store\ProductPropertyGroup;

class RetailCrmSyncProperties extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retail:properties:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Синхронизация аттрибутов из Retail CRM';

    public function __construct(protected RetailCrmService $retailCRMService)
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        if (!Category::query()->whereNotNull('external_id')->exists()) {
            $this->error("Нет категорий из RetailCRM. Не с чем синхронизировать!");
            return self::FAILURE;
        }

        try {
            $retailProperties = $this->retailCRMService->getProductsProperties();

            foreach ($retailProperties as $retailProperty) {
                /** @var Property $property */
                $property = Property::query()->firstOrNew(['code' => $retailProperty->code], ['title' => $retailProperty->name, 'visible' => $retailProperty->visible]);
                $property->save();

                $propertyRetailCategoriesIds = collect($retailProperty->groups)->map(fn(ProductPropertyGroup $propertyGroup) => $propertyGroup->id)->toArray();
                $innerCategoriesIds = Category::query()->whereIn('external_id', $propertyRetailCategoriesIds)->pluck('id');

                $property->categories()->sync($innerCategoriesIds);
            }

        } catch (RetailCrmServiceException $e) {
            $this->error($e->getMessage());
            return self::FAILURE;
        }

        return self::SUCCESS;
    }
}
