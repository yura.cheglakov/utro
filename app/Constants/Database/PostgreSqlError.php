<?php

namespace App\Constants\Database;

class PostgreSqlError
{
    const UNIQUE_VIOLATION = "23505";
}
