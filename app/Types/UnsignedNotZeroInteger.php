<?php

namespace App\Types;

use UnexpectedValueException;

class UnsignedNotZeroInteger
{
    private int $value;

    public function __construct(int $value)
    {
        if (0 >= $value) {
            throw new UnexpectedValueException(static::class . " значение должно быть больше 0");
        }

        $this->value = $value;
    }

    public function value(): int
    {
        return $this->value;
    }
}