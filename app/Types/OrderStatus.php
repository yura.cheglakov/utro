<?php

namespace App\Types;

final class OrderStatus
{
    private function __construct(
        private readonly string $value
    ) {
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function equals(self $status): bool
    {
        return $this->value === $status->value;
    }

    public static function fromString(string $status): self
    {
        return match ($status) {
            'new' => self::new(),
            'approval' => self::approval(),
            'assembling' => self::assembling(),
            'delivery' => self::delivery(),
            'completed' => self::completed(),
            'canceled' => self::canceled(),
            'returned' => self::returned(),
            default => throw new \InvalidArgumentException("Некорректный статус заказа $status")
        };
    }

    public static function new(): OrderStatus
    {
        return new self('new');
    }

    public static function approval(): OrderStatus
    {
        return new self('approval');
    }

    public static function assembling(): OrderStatus
    {
        return new self('assembling');
    }

    public static function delivery(): OrderStatus
    {
        return new self('delivery');
    }

    public static function completed(): OrderStatus
    {
        return new self('completed');
    }

    public static function canceled(): OrderStatus
    {
        return new self('canceled');
    }

    public static function returned(): OrderStatus
    {
        return new self('returned');
    }
}