<?php

namespace App\Types;

class UnsignedInteger
{
    private int $value;

    public function __construct(int $value)
    {
        if (0 > $value) {
            throw new \UnexpectedValueException(static::class . " значение не может быть меньше 0");
        }

        $this->value = $value;
    }

    public function value(): int
    {
        return $this->value;
    }
}