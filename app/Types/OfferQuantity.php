<?php

namespace App\Types;

final class OfferQuantity extends UnsignedInteger
{
    public function add(self $quantity): self
    {
        return new self($this->value() + $quantity->value());
    }

    public function subtract(self $quantity): self
    {
        return new self($this->value() - $quantity->value());
    }
}