<?php

namespace App\Casts;

use App\Types\OfferQuantity;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Database\Eloquent\Model;

class OfferQuantityCast implements CastsAttributes
{
    /**
     * Cast the given value.
     *
     * @param  Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return OfferQuantity
     */
    public function get($model, string $key, $value, array $attributes): OfferQuantity
    {
        return new OfferQuantity($value);
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return array
     */
    public function set($model, string $key, $value, array $attributes): array
    {
        if (!($value instanceof OfferQuantity)) {
            throw new \InvalidArgumentException("Переданное значение не OfferQuantity");
        }

        return [
            'quantity' => $value->value()
        ];
    }
}
