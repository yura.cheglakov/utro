<?php

namespace App\Casts;

use App\Types\OrderStatus;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Database\Eloquent\Model;

class OrderStatusCast implements CastsAttributes
{
    /**
     * Cast the given value.
     *
     * @param  Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return OrderStatus
     */
    public function get($model, string $key, $value, array $attributes): OrderStatus
    {
        if (is_null($value)) {
            throw new \InvalidArgumentException("Статус заказа не может быть null");
        }

        return OrderStatus::fromString($value);
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return string
     */
    public function set($model, string $key, $value, array $attributes): string
    {
        if (!($value instanceof OrderStatus)) {
            throw new \InvalidArgumentException("Переданное значение не является OrderStatus");
        }

        return (string) $value;
    }
}
